import type { Site, SocialObjects } from "./types";

export const SITE: Site = {
  website: "https://blog.amaterath.com/", // replace this with your deployed domain
  author: "Yuni Hutsuka",
  profile: "https://blog.amaterath.com/posts/about-me/",
  desc: "Yuni's Website",
  title: "Studio Amaterath",
  ogImage: "",
  lightAndDarkMode: true,
  postPerIndex: 5,
  postPerPage: 5,
  scheduledPostMargin: 15 * 60 * 1000, // 15 minutes
  showArchives: true,
  editPost: {
    url: "https://twitter.com/intent/tweet?text=%40yuni_hutsuka%0A%E8%AA%A4%E5%AD%97%E8%84%B1%E5%AD%97%E3%81%82%E3%82%8B%E3%82%88%EF%BC%81",
    text: "Point out Typos",
    appendFilePath: false,
  },
};

export const LOCALE = {
  lang: "ja", // html lang code. Set this empty and default will be "en"
  langTag: ["ja-JP"], // BCP 47 Language Tags. Set this empty [] to use the environment default
} as const;

export const LOGO_IMAGE = {
  enable: false,
  svg: true,
  width: 216,
  height: 46,
};

export const SOCIALS: SocialObjects = [
  {
    name: "Mail",
    href: "mailto:yuni.wille999@gmail.com",
    linkTitle: `Send an email to Yuni Hutsuka`,
    active: true,
  },
  {
    name: "Discord",
    href: "https://discordapp.com/users/466475544393941002",
    linkTitle: `Yuni Hutsuka on Discord`,
    active: true,
  },
  {
    name: "Github",
    href: "https://github.com/yuni-hutsuka",
    linkTitle: `Yuni Hutsuka on Github`,
    active: true,
  },
  {
    name: "X",
    href: "https://twitter.com/yuni_hutsuka",
    linkTitle: `Yuni Hutsuka on Twitter`,
    active: true,
  },
];
